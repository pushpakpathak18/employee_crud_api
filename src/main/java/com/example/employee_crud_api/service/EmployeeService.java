package com.example.employee_crud_api.service;

import com.example.employee_crud_api.entity.Employee;
import com.example.employee_crud_api.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository repository;

    public Employee addEmployee(Employee employee)
    {

        return repository.save(employee);
    }

    /*public List<Employee> addEmployees(List<Employee> employees)
    {
        return repository.saveAll(employees);
    }     */

    public List<Employee> getEmployees()
    {
        return repository.findAll();
    }

    public Employee getEmployeeById(int id)
    {
        return repository.findById(id).orElse(null);
    }

    public String deleteEmployee(int id)
    {
        repository.deleteById(id);
        return "Employee" + id + "Deleted !!!";
    }

    public Employee updateEmployee(Employee employee)
    {
        Employee existingEmp=repository.findById(employee.getEmp_id()).orElse(null);
        existingEmp.setEmp_name(employee.getEmp_name());
        existingEmp.setEmp_email(employee.getEmp_email());
        existingEmp.setEmp_salary(employee.getEmp_salary());
        return repository.save(existingEmp);
    }


}
