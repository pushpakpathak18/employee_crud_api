package com.example.employee_crud_api.controller;

import com.example.employee_crud_api.entity.Employee;
import com.example.employee_crud_api.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService service;

    @PostMapping("/addEmployee")
    public Employee addEmployee(@RequestBody Employee employee){
        return service.addEmployee(employee);
    }

    @GetMapping("/employees")
    public List<Employee> findAllEmployees()
    {
        return service.getEmployees();
    }

    @GetMapping("employee/{id}")
    public Employee findEmployeeById(@PathVariable int id)
    {
        return service.getEmployeeById(id);
    }

    @PutMapping("/updateEmployee")
    public Employee updateEmployee(@RequestBody Employee employee){
        return service.updateEmployee(employee);
    }

    @DeleteMapping("/deleteEmployee/{id}")
    public String deleteEmployee(@PathVariable int id)
    {
        return service.deleteEmployee(id);
    }

}
