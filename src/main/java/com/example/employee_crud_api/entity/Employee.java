package com.example.employee_crud_api.entity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "EMPLOYEE")

public class Employee {

    @Id
    @GeneratedValue
    private int emp_id;
    private String emp_name;
    private String emp_email;
    private double emp_salary;
}
